<?php
include ('db_connect.php');

$id = 0;

// Retrieve records
$sql = "SELECT * FROM explorer";
$res = $conn->query($sql);
$status_name = ['not started', 'in progress', 'finished'];

// Update records
if (isset($_POST['update']))
{
  $title = trim($_POST['title']);
  $title = htmlspecialchars($title);
  $status = trim($_POST['status']);

  $title = mysqli_real_escape_string($conn, $title);
  $id = mysqli_real_escape_string($conn, $_POST['id']);
  $conn->query("UPDATE explorer SET status='$status', title='$title' WHERE id=$id");
  header("location: index.php");
}

// Delete records
if (isset($_GET['del']))
{
  $id = $_GET['del'];
  $conn->query("DELETE FROM explorer WHERE id=$id");
  header("location: index.php");
}

if (isset($_GET['edit']))
{
  $id = $_GET['edit'];

  $rec = $conn->query("SELECT * FROM explorer WHERE id=$id");
  $record = mysqli_fetch_array($rec);
  $title = $record['title'];
  $id = $record['id'];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#2d89ef">
  <meta name="theme-color" content="#ffffff">
  <!-- Stylesheets -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="styles/main.css">
  <title>WordStat</title>
</head>

<body>
  <header>
    <nav class="nav">
      <a href="/" class="logo-box">
        <img src="img/Logo.jpg" class="logo-box_img">
        <div class="logo-box_text">WorkStat</div>
      </a>
      <form method="GET" action="microservices/explorer.php">
        <div class="search-bar">
          <input type="search" name="query">
          <img class="search-btn_icon" src="img/icon.png" alt="search">
        </div>
        <div class="submit-btn">
          <input name="submit" type="submit" value="submit">
          <img class="submit-btn_icon" src="img/icon.png" alt="search">
        </div>
      </form>
    </nav>
  </header>
  <div class="main-content">
    <div class="main-nav">
      <ul class="breadcrumbs">
        <li>
          <a href="/">Home <i class="material-icons chevron_right">chevron_right</i></a>
        </li>
        <li>
          <a href="/pages/vacancy.php">Vacancies <i class="material-icons chevron_right">chevron_right</i></a>
        </li>
        <li>
          <a href="/pages/words.php">Words</a>
        </li>
      </ul>
    </div>
  </div>
  <main>
    <div class="main-content">
      <table class="explorer">
        <thead>
          <tr>
            <th>id</th>
            <th>query</th>
            <th>status</th>
            <th>title</th>
            <th>date</th>
            <th>action</th>
          </tr>
        </thead>
        <tbody>
          <?php if ($res->num_rows > 0): ?>
          <?php while ($row = $res->fetch_assoc()): ?>
          <tr class="row">
            <td>
              <?= $row['id']; ?>
            </td>
            <td><a class="query-link" target="_blank" href="<?= $row['query']; ?>">
                <?= $row['query']; ?></a></td>
            <td>
              <?= $status_name[$row['status']]; ?>
            </td>
            <td>
              <?= $row['title']; ?>
            </td>
            <td>
              <?= $row['create_date']; ?>
            </td>
            <td>
              <a href="?edit=<?= $row['id']; ?>#popup" class="btn edit_btn">
                <img src="img/edit.png" alt="edit">
              </a>
              <a href="?del=<?= $row['id']; ?>" class="btn delete_btn">
                <img src="img/delete.png" alt="delete">
              </a>
            </td>
          </tr>
          <?php endwhile; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </main>
  <div class="popup" id="popup">
    <div class="popup__content">
      <a href="#" class="popup__close">&times;</a>
      <form class="popup__form" method="post" action="<?= $_SERVER['PHP_SELF']?>">
        <input type="hidden" name="id" value="<?= $id; ?>">
        <div class="input-group">
          <label>Status</label>
          <select name="status">
            <option value="0">not started</option>
            <option value="1">in progress</option>
            <option value="2">finished</option>
          </select>
        </div>
        <div class="input-group">
          <label>Title </label>
          <input type="text" name="title" value="<?= $title; ?>">
        </div>
        <div class="input-group">
          <button type="submit" name="update">Save</button>
        </div>
      </form>
    </div>
  </div>
</body>

</html>