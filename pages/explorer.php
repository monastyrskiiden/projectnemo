<?php
include ('../db_connect.php');

// Get Data from DB
$sql = "SELECT * FROM explorer WHERE status='0'";
$res = $conn->query($sql);
$status = ['not started', 'in progress', 'finished'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#2d89ef">
  <meta name="theme-color" content="#ffffff">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../styles/main.css">
  <title>Explorer</title>
</head>
<body>
  <header>
    <nav class="nav">
      <a href="/" class="logo-box">
        <img src="../img/Logo.jpg" class="logo-box_img">
        <div class="logo-box_text">WorkStat</div>
      </a>
      <form method="GET" action="../microservices/explorer.php">
        <div class="search-bar">
          <input type="search" name="query">
          <img class="search-btn_icon" src="../img/icon.png" alt="search">
        </div>
        <div class="submit-btn">
          <input name="submit" type="submit" value="submit">
          <img class="submit-btn_icon" src="../img/icon.png" alt="search">
        </div>
      </form>
    </nav>
  </header>
  <main>
    <div class="main-content">
      <table class="explorer">
        <thead>
          <tr>
            <th>id</th>
            <th>query</th>
            <th>status</th>
            <th>title</th>
            <th>date</th>
          </tr>
        </thead>
        <tbody>
        <?php if ($res->num_rows > 0): ?>
          <?php while ($row = $res->fetch_assoc()): ?>        
            <tr class="row">
              <td><?=$row['id']; ?></td>
              <td><a class="query-link" target="_blank" href="<?=$row['query']; ?>"><?=$row['query']; ?></a></td>
              <td><?=$status[$row['status']]; ?></td>
              <td><?=$row['title']; ?></td>
              <td><?=$row['create_date']; ?></td>
            </tr>
          <?php endwhile; ?>
        <?php endif; ?>    
        </tbody>
      </table>
      <a href="../microservices/vacancy.php" class="btn__proceed">Proceed</a>
    </div>
  </main>
</body>
</html>
