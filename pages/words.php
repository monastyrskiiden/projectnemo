<?php
include_once ('../db_connect.php');

// Select all words and their quantity
$sql = "SELECT words.id, words.word, words.type, words.create_date, SUM(vacancy_desc_to_words.qty) AS qty
          FROM words, vacancy_desc_to_words
          WHERE words.id = vacancy_desc_to_words.words_id
          GROUP BY words_id
          ORDER BY qty DESC";
$words = $conn->query($sql);

// Delete records
if (isset($_GET['del']))
{
  $id = $_GET['del'];
  $conn->query("DELETE FROM words WHERE id=$id");
  header("location: words.php");
}

// Update words status
if (isset($_POST))
{
  $skills = $_POST;
  foreach ($skills as $index => $value)
  {
    $sql = "UPDATE words SET type=1 WHERE id=$index";
    $conn->query($sql);
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#2d89ef">
  <meta name="theme-color" content="#ffffff">
  <!-- Stylesheets -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="../styles/main.css">
  <title>WordStat</title>
</head>

<body>
  <main>
    <div class="main-content">
      <h1 class="main-header">Choose skills!</h1>
      <div class="main-nav">
        <ul class="breadcrumbs">
          <li>
            <a href="/">Home <i class="material-icons chevron_right">chevron_right</i></a>
          </li>
          <li>
            <a href="/pages/vacancy.php">Vacancies <i class="material-icons chevron_right">chevron_right</i></a>
          </li>
          <li>
            <a href="/pages/words.php">Words</a>
          </li>
        </ul>
      </div>
      <form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST">
        <table class="explorer">
          <thead>
            <tr>
              <th>skill</th>
              <th>id</th>
              <th>word</th>
              <th>quantity</th>
              <th>date</th>
              <th>action</th>
            </tr>
          </thead>
          <tbody>
          <?php if ($words->num_rows > 0): ?>
            <?php while ($row = $words->fetch_assoc()): ?>
            <tr class="row">
              <td>
                <?php if ($row['type'] == 1): ?>
                  <input type="checkbox" checked name="<?=$row['id']; ?>" value="skill">
                <?php else: ?>
                  <input type="checkbox" name="<?=$row['id']; ?>" value="skill">
                <?php endif; ?>
              </td>
              <td>
                <?=$row['id']; ?>
              </td>
              <td>
                <?=$row['word']; ?>
              </td>
              <td>
                <?=$row['qty']; ?>
              </td>
              <td>
                <?=$row['create_date']; ?>
              </td>
              <td>
                <a href="?del=<?=$row['id']; ?>" class="btn delete_btn">
                  <img src="../img/delete.png" alt="delete">
                </a>
              </td>
            </tr>
            <?php endwhile; ?>
          <?php endif; ?>
          </tbody>
        </table>
        <button type="submit" class="btn__save">Save</button>
      </form>
    </div>
  </main>
</body>

</html>