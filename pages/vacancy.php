<?php
include ('../db_connect.php');

// Retrieve records
$sql = "SELECT * FROM vacancy";
$res = $conn->query($sql);
$status_name = ['not started', 'in progress', 'finished'];

// Update records
if (isset($_POST['update']))
{
  // Fields validation
  $status = htmlspecialchars(trim($_POST['status']));
  $title = htmlspecialchars(trim($_POST['title']));
  $id = htmlspecialchars(trim($_POST['id']));
  $company = htmlspecialchars(trim($_POST['company']));
  $location = htmlspecialchars(trim($_POST['location']));

  $title = mysqli_real_escape_string($conn, $title);
  $id = mysqli_real_escape_string($conn, $id);
  $company = mysqli_real_escape_string($conn, $company);
  $location = mysqli_real_escape_string($conn, $location);

  $conn->query("UPDATE vacancy SET vacancy_status='$status', company_name='$company', location='$location', vacancy_title='$title' WHERE id=$id");
  header("location: vacancy.php");
}

// Delete records
if (isset($_GET['del']))
{
  $id = $_GET['del'];
  $conn->query("DELETE FROM vacancy WHERE id=$id");
  header("location: vacancy.php");
}

if (isset($_GET['edit']))
{
  $id = $_GET['edit'];

  $rec = $conn->query("SELECT * FROM vacancy WHERE id=$id");
  $record = mysqli_fetch_array($rec);
  $title = $record['vacancy_title'];
  $company = $record['company_name'];
  $location = $record['location'];
  $id = $record['id'];

}

// Get vacancy description
if (isset($_GET['desc']))
{
  $id = $_GET['desc'];
  $row = $conn->query("SELECT description FROM vacancy_description WHERE vacancy_id=$id");
  while ($desc = $row->fetch_assoc())
  {
    $description = $desc['description'];
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#2d89ef">
  <meta name="theme-color" content="#ffffff">
  <!-- Stylesheets -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="../styles/main.css">
  <title>Vacancies</title>
</head>
<body>
  <header>
    <nav class="nav">
      <a href="/" class="logo-box">
        <img src="../img/Logo.jpg" class="logo-box_img">
        <div class="logo-box_text">WorkStat</div>
      </a>
      <form method="GET" action="../microservices/explorer.php">
        <div class="search-bar">
          <input type="search" name="query">
          <img class="search-btn_icon" src="../img/icon.png" alt="search">
        </div>
        <div class="submit-btn">
          <input name="submit" type="submit" value="submit">
          <img class="submit-btn_icon" src="../img/icon.png" alt="search">
        </div>
      </form>
    </nav>
  </header>
  <div class="main-content">
    <div class="main-nav">
      <ul class="breadcrumbs">
        <li>
          <a href="/">Home <i class="material-icons chevron_right">chevron_right</i></a>
        </li>
        <li>
          <a href="/pages/vacancy.php">Vacancies <i class="material-icons chevron_right">chevron_right</i></a>
        </li>
        <li>
          <a href="/pages/words.php">Words</a>
        </li>
      </ul>
    </div>
  </div>
  <main>
    <div class="main-content">
      <table class="explorer">
        <thead>
          <tr>
            <th>id</th>
            <th>vacancy_url</th>
            <th>title</th>
            <th>company</th>
            <th>location</th>
            <th>status</th>
            <th>acion</th>
          </tr>
        </thead>
        <tbody>
        <?php if ($res->num_rows > 0): ?>
          <?php while ($row = $res->fetch_assoc()): ?>        
            <tr class="row">
              <td><?=$row['id']; ?></td>
              <td><a class="query-link" target="_blank" href="<?=$row['vacancy_url']; ?>"><?=$row['vacancy_url']; ?></a></td>
              <td class="title"><?=$row['vacancy_title']; ?></td>
              <td class="company"><?=$row['company_name']; ?></td>
              <td class="location"><?=$row['location']; ?></td>
              <td class="status"><?=$status_name[$row['vacancy_status']]; ?></td>
              <td>
                <a href="?edit=<?=$row['id']; ?>#popup" class="btn edit_btn">
                  <img src="../img/edit.png" alt="edit">
                </a>
                <a href="?del=<?=$row['id']; ?>" class="btn delete_btn">
                  <img src="../img/delete.png" alt="delete">
                </a>
                <a href="?desc=<?=$row['id'] ?>#description" class="btn">
                  <img src="../img/show.png" alt="delete">
                </a>
              </td>
            </tr>
          <?php endwhile; ?>
        <?php endif; ?>    
        </tbody>
      </table>
      <a href="../microservices/words.php" class="btn__proceed">Proceed</a>
    </div>
  </main>
  <div class="popup" id="popup">
    <div class="popup__content">
      <a href="#" class="popup__close">&times;</a>
      <form class="popup__form" method="post" action="<?=$_SERVER['PHP_SELF'] ?>">
        <input type="hidden" name="id" value="<?=$id; ?>">
        <div class="input-group">
          <label>Status</label>
          <select name="status">
            <option value="0">not started</option>
            <option value="1">in progress</option>
            <option value="2">finished</option>
          </select>
        </div>
        <div class="input-group">
          <label>Title </label>
          <input type="text" name="title" value="<?=$title; ?>">
        </div>
        <div class="input-group">
          <label>Company </label>
          <input type="text" name="company" value="<?=$company; ?>">
        </div>
        <div class="input-group">
          <label>Location </label>
          <input type="text" name="location" value="<?=$location; ?>">
        </div>
        <div class="input-group">
          <button type="submit" name="update">Save</button>
        </div>
      </form>
    </div>
  </div>
  <div class="description" id="description">
    <div class="description__content">
      <a href="#" class="description__close">&times;</a>
      <p><?=$description; ?></p>
    </div>
  </div>
</body>
</html>