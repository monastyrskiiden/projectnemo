<?php
include ('../db_connect.php');
include ('../libs/simple_html_dom.php');

// Select all vacancies from DB
$vacancies = $conn->query("SELECT * FROM vacancy WHERE vacancy_status='0'");

// Change explorer status
$change_status = "UPDATE explorer SET status='1' WHERE status='0'";
$conn->query($change_status);
// Change vacancy status
$change_status = "UPDATE vacancy SET vacancy_status='1' WHERE vacancy_status='0'";
$conn->query($change_status);

// Extract data about every vacancy
if ($vacancies->num_rows > 0)
{
  while ($row = $vacancies->fetch_assoc())
  {
    $vacancy_id = $row['id'];
    $url = $row['vacancy_url'];

    // Open vacancy page
    $html = file_get_contents($url);
    $html = str_get_html($html);
    // Description starts
    $first_el = $html->find('h2', 0);
    $desc = $first_el->next_sibling();
    $desc_dump = '';

    // Loop through vacancy description
    while ($desc)
    {
      // If TRUE description ends
      if (($desc->class) === 'form-group hidden-print')
      {
        break;
      }
      $desc_dump .= $desc;
      $desc = $desc->next_sibling();
    }

    // Send Data to Vacancy_description
    $desc_dump = mysqli_real_escape_string($conn, $desc_dump);
    $sql_vacancy_desc = "INSERT INTO vacancy_description (vacancy_id, description, status) VALUES ($vacancy_id, '$desc_dump', '0')";
    if (!$conn->query($sql_vacancy_desc) === true)
    {
      echo "Connection error!";
    }
  }
}

// Change explorer status
$change_status = "UPDATE explorer SET status='2'";
$conn->query($change_status);
// Change vacancy status
$change_status = "UPDATE vacancy SET vacancy_status='2'";
$conn->query($change_status);

header('location: ../pages/vacancy.php');
exit;