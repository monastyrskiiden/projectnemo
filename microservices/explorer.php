<?php
include_once ('../db_connect.php');
include ('../libs/simple_html_dom.php');

// Params
if (isset($_GET['query']))
{
  $query = trim($_GET['query']);
  $query = htmlspecialchars($query);
  $query_title = $query;
}

$main_url = 'https://www.work.ua/jobs-' . urlencode($query) . '/';
$page = file_get_contents($main_url);
$html = str_get_html($page);

// Check if page has pagination
if (NULL !== ($html->find('.pagination', 0)))
{
  $last_page = $html->find('.pagination', 0)
    ->find('li', -2)->plaintext;
}
else
{
  $last_page = 1;
}

$sql_vacancy = [];

// Get URLs of pages
for ($i = 1;$i <= $last_page;$i++)
{
  $url = "$main_url?page=$i";
  $page = file_get_contents($url);
  $html = str_get_html($page);
  $sql = "INSERT INTO explorer (query, title) VALUES ('$url', '$query_title')";

  // Send Data to Explorer
  if (!$conn->query($sql) === true)
  {
    echo 'Connection error!';
  }
  $last_id = $conn->insert_id;

  // Get Data about vacancies
  foreach ($html->find('div.job-link') as $obj)
  {
    $link = $obj->find('h2 a');
    $company = $obj->find('span > b', 0);

    foreach ($link as $key)
    {
      $link_loc = $key->href;
      $company_loc = $company->plaintext;
      $title_loc = $key->plaintext;

      // Get Location
      $html = file_get_contents("https://www.work.ua$link_loc");
      $html = str_get_html($html);
      $location_arr = $html->find('dl');

      foreach ($location_arr as $loc)
      {
        $dt_arr = $loc->find('dt');

        foreach ($dt_arr as $dt)
        {
          $text = $dt->plaintext;
          if ($text === "Город:")
          {
            $location = $dt->next_sibling()->plaintext;
          }
        }
      }
      $sql_vacancy[] = "($last_id, 'https://www.work.ua$link_loc', '$company_loc', '$location', '$title_loc')";
    }
  }
}

// Send Data to Vacancy
$sql_vacancy = "INSERT INTO vacancy (explorer_id, vacancy_url, company_name, location, vacancy_title) VALUES " . implode(',', $sql_vacancy);
if (!$conn->query($sql_vacancy) === true)
{
  echo 'Connection error!';
}

$html->clear();
unset($html);

header('location: ../pages/explorer.php');
exit;