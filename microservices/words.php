<?php
include ('../db_connect.php');
include ('../libs/simple_html_dom.php');

$sql = "SELECT description, id FROM vacancy_description WHERE status='0'";
$desc_arr = $conn->query($sql);

// Change vacancy_description status
$change_status = "UPDATE vacancy_description SET status='1'";
$conn->query($change_status);

if ($desc_arr->num_rows > 0)
{
  while ($row = $desc_arr->fetch_assoc())
  {

    $description = $row['description'];
    $vacancy_id = $row['id'];

    // Remove all tags and special characters
    $description = strip_tags($description);
    $description = preg_replace('%[^A-Za-zА-Яа-я0-9\s]%u', ' ', $description);

    // Remove &nbsp; symbols
    $description = str_replace("\xc2\xa0", ' ', $description);

    // Transform text to lowercase and remove 2 or more spaces
    $description = mb_strtolower($description);
    $description = preg_replace('/ {2,}/', ' ', $description);

    // Creating array of words
    $words_arr = explode(' ', $description);

    // Calculate words
    $words_qty = array_count_values($words_arr);

    foreach ($words_qty as $word => $qty)
    {
      // Select word from DB
      $get_word = "SELECT * FROM words WHERE word = '$word'";
      $result = $conn->query($get_word);

      // Fetch Data
      if ($result->num_rows > 0)
      {
        while ($row = $result->fetch_assoc())
        {
          // Check if word already in DB
          if ($row['word'] === "$word")
          {
            $word_id = $row['id'];

            $sql_qty = "INSERT INTO vacancy_desc_to_words (vacancy_description_id, words_id, qty) VALUES ($vacancy_id, $word_id, $qty)";
            $conn->query($sql_qty);
          }
        }
      }
      else
      {
        // If it's not, save word to DB
        $insert_words = "INSERT INTO words (word) VALUES ('$word')";
        if ($conn->query($insert_words) === true)
        {
          $word_id = $conn->insert_id;

          $sql_qty = "INSERT INTO vacancy_desc_to_words (vacancy_description_id, words_id, qty) VALUES ($vacancy_id, $word_id, $qty)";
          $conn->query($sql_qty);
        }
      }
    }
  }
}

// Change vacancy_description status
$change_status = "UPDATE vacancy_description SET status='2'";
$conn->query($change_status);

header('location: ../pages/words.php');
exit;